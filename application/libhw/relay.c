
#include "GPIO.h"

#include "relay.h"

#define RELAY_PIN   (1 << 4)

void Relay_ctrl(bool new_state) {
#if GPIO_ENABLED
    if (!(gpio_port_get_dir(GPIO_PORTA) & RELAY_PIN))
        gpio_port_set_dir(GPIO_PORTA, RELAY_PIN, 0);

    gpio_port_set_val(GPIO_PORTA,
                      new_state ? RELAY_PIN : 0,
                      new_state ? 0 : RELAY_PIN);
#endif
}
